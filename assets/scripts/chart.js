try{
  var options = {
    responsive: true,
    maintainAspectRatio: false,
    spanGaps: false,
    title: {
      display: false,
    },
    legend: {
      display: false,
      labels: {
        fontFamily: 'Tajawal',
      }
    },
    elements: {
      line: {
        tension: 0.000001
      }
    },
    plugins: {
      filler: {
        propagate: false
      }
    },
    scales: {
      xAxes: [{
        ticks: {
  
          autoSkip: false,
          maxRotation: 0,
          fontFamily: 'Tajawal',
          fontSize: 9
        }
      }]
    }
  }
  var bgColors = [
    'rgba(255, 99, 132, 0.2)',
    'rgba(54, 162, 235, 0.2)',
    'rgba(255, 206, 86, 0.2)',
    'rgba(75, 192, 192, 0.2)',
    'rgba(153, 102, 255, 0.2)',
    'rgba(255, 159, 64, 0.2)',
    'rgba(255, 99, 132, 0.2)',
    'rgba(54, 162, 235, 0.2)',
    'rgba(255, 206, 86, 0.2)',
    'rgba(75, 192, 192, 0.2)',
    'rgba(153, 102, 255, 0.2)',
    'rgba(255, 159, 64, 0.2)',
  ]
  var borderColors = [
    'rgba(255, 99, 132, 1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)',
    'rgba(255, 99, 132, 1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)'
  ]
  
  var ctx = document.getElementById('barChart').getContext('2d');
  var ctx = document.getElementById('lineChart').getContext('2d')
  var ctx = document.getElementById('horizontalBars').getContext('2d')
  var ctx = document.getElementById('donutChart').getContext('2d')
  
  var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: aofi.chart1.labels,
      datasets: [{
        label: '',
        data: aofi.chart1.data,
        backgroundColor: bgColors,
        borderColor: borderColors,
        borderWidth: 1
      }]
    },
    options: options
  });
  
  var myLineChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: aofi.chart2.labels,
      datasets: [{
        label: '',
        data: aofi.chart2.data,
        backgroundColor: '#f3f3f3',
        borderColor: '#FF7376',
      }],
    },
    options: options
  });
  
  var myLineChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
      labels: aofi.chart3.labels,
      datasets: [{
        label: '',
        data: aofi.chart3.data,
        backgroundColor: bgColors,
        borderColor: borderColors,
        borderWidth: 1
      }],
    },
    options: options
  });
  
  var myLineChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
      labels: aofi.chart4.labels,
      datasets: [{
        label: '',
        data: aofi.chart4.data,
        backgroundColor: bgColors,
        borderColor: borderColors,
        borderWidth: 1
      }],
    },
  });
} catch(err){
  console.log('charts not found')
}