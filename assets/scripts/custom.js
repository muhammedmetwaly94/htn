$(document).ready(function () {

  $('.custom_select').select2()

  // file input bs
  $(".inputFile").fileinput({
    uploadUrl: "/site/upload-file-chunks",
    enableResumableUpload: true,
    rtl: true,
    showUpload: false,
    showPause: false,
    theme: 'fas',
    fileActionSettings: {
      showZoom: function (config) {
        if (config.type === 'pdf' || config.type === 'image') {
          return true;
        }
        return false;
      }
    }
  });

  $('.repeated_form button').click(function () {
    const firstEl = $(this).siblings('.form-row:first')
    const cloned = firstEl.clone()

    cloned.removeClass('d-none').insertAfter($(this).siblings('.form-row:last'))
    cloned.find('.bootstrap-tagsinput').remove()
    cloned.find('input').val('')
  })

  $('body').on('click', '.remove_el', function () {
    $(this).parents('.single_item').remove()
  })

  $('body').on('keypress', 'form input', (function (event) {
    if (event.keyCode == 13) {
      event.preventDefault();
    }
  }));
  // datatable
  $('.dataTable').dataTable();

  $('.header_btn').click(function(e){
    $('.bottom_nav').toggleClass('show')
    e.stopPropagation()
  })
  $('body').click(function(){
    if($('.bottom_nav').hasClass('show')){
      $('.bottom_nav').removeClass('show')
    }
  })

  $('#all_checks').change(function(){
    if($(this).prop('checked')){
      console.log('true')
      $('.checkbox_custom input').not(this).prop('checked', true)
    } else {
      $('.checkbox_custom input').not(this).prop('checked', false)
    }
  })

  $('#visitResult').change(function(){
    showVisitForm(this)
  })
  function showVisitForm(selectElm){
    if($(selectElm).children('option:selected').data('form')){
      $('#visitForm').removeClass('d-none')
    } else {
      $('#visitForm').addClass('d-none')
    }
  }
  showVisitForm($('#visitResult'))
});
// notification toaster
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": true,
  "positionClass": "toast-bottom-center",
  "preventDuplicates": false,
  "onclick": null,
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}